<?php

namespace Imoje\Payment;

use Exception;

/**
 * Class Api
 *
 * @package Imoje\Payment
 */
class Api
{

	const TRANSACTION = 'transaction';
	const MERCHANT = 'merchant';
	const SERVICE = 'service';
	const TRANSACTION_TYPE_SALE = 'sale';
	const TRANSACTION_TYPE_REFUND = 'refund';

	/**
	 * @var array
	 */
	private static $serviceUrls = [
		Util::ENVIRONMENT_PRODUCTION => 'https://api.imoje.pl/v1',
		Util::ENVIRONMENT_SANDBOX    => 'https://sandbox.api.imoje.pl/v1',
	];

	/**
	 * @var string
	 */
	private $authorizationToken;

	/**
	 * @var string
	 */
	private $merchantId;

	/**
	 * @var string
	 */
	private $serviceId;

	/**
	 * @var string
	 */
	private $environment;

	/**
	 * Api constructor.
	 *
	 * @param string $authorizationToken
	 * @param string $merchantId
	 * @param string $serviceId
	 * @param string $environment
	 */
	public function __construct($authorizationToken, $merchantId, $serviceId, $environment = '')
	{

		$this->authorizationToken = $authorizationToken;
		$this->merchantId = $merchantId;
		$this->serviceId = $serviceId;

		if(!$environment) {
			$environment = Util::ENVIRONMENT_PRODUCTION;
		}
		$this->environment = $environment;
	}

	/**
	 * @param string $string
	 *
	 * @return array
	 */
	public static function parseStringToArray($transaction)
	{
		$array = [];

		if($transaction['action']['method'] === Util::METHOD_REQUEST_POST) {
			parse_str($transaction['action']['contentBodyRaw'], $array);
		}

		if($transaction['action']['method'] === Util::METHOD_REQUEST_GET) {
			$urlParsed = parse_url($transaction['action']['url']);

			if(isset($urlParsed['query']) && $urlParsed['query']) {
				parse_str($urlParsed['query'], $array);
			}
		}

		return $array;
	}

	/**
	 * @param int    $amount
	 * @param string $currency
	 * @param string $orderId
	 * @param string $paymentMethod
	 * @param string $paymentMethodCode
	 * @param string $successReturnUrl
	 * @param string $failureReturnUrl
	 * @param string $customerFirstName
	 * @param string $customerLastName
	 * @param string $customerEmail
	 * @param string $clientIp
	 * @param string $type
	 *
	 * @return string
	 */
	public function prepareData(
		$amount,
		$currency,
		$orderId,
		$paymentMethod,
		$paymentMethodCode,
		$successReturnUrl,
		$failureReturnUrl,
		$customerFirstName,
		$customerLastName,
		$customerEmail,
		$type = 'sale',
		$clientIp = '',
		$blikCode = ''
	) {

		if(!$clientIp) {
			$clientIp = $_SERVER['REMOTE_ADDR'];
		}

		$array = [
			'type'              => $type,
			'serviceId'         => $this->serviceId,
			'amount'            => $amount,
			'currency'          => $currency,
			'orderId'           => (string) $orderId,
			'title'             => (string) $orderId,
			'paymentMethod'     => $paymentMethod,
			'paymentMethodCode' => $paymentMethodCode,
			'successReturnUrl'  => $successReturnUrl,
			'failureReturnUrl'  => $failureReturnUrl,
			'clientIp'          => $clientIp,
			'customer'          => [
				'firstName' => $customerFirstName,
				'lastName'  => $customerLastName,
				'email'     => $customerEmail,
			],
		];

		if($blikCode) {
			$array['blikCode'] = $blikCode;
		}

		return json_encode($array);
	}

	/**
	 * @param int $amount
	 *
	 * @return string
	 */
	public function prepareRefundData(
		$amount
	) {

		return json_encode([
			'amount'    => $amount,
			'serviceId' => $this->serviceId,
			'type'      => self::TRANSACTION_TYPE_REFUND,
		]);
	}

	/**
	 * @param string $body
	 *
	 * @return array
	 */
	public function createTransaction($body)
	{
		return $this->call(
			$this->getTransactionCreateUrl(),
			Util::METHOD_REQUEST_POST,
			$body
		);
	}

	/**
	 * @param string $url
	 * @param string $methodRequest
	 * @param string $body
	 *
	 * @return array
	 */
	private function call($url, $methodRequest, $body)
	{

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $methodRequest);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'Authorization: Bearer ' . $this->authorizationToken,
		]);

		$resultCurl = json_decode(curl_exec($curl), true);

		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		if(($httpCode !== 200) || !$resultCurl) {

			$array = [
				'success' => false,
				'data'    => [
					'httpCode' => $httpCode,
					'error'    => curl_error($curl),
					'body'     => '',
				],
			];

			if(isset($resultCurl['apiErrorResponse']['message']) && $resultCurl['apiErrorResponse']['message']) {
				$array['data']['body'] = $resultCurl['apiErrorResponse']['message'];
			}

			return $array;
		}

		return [
			'success' => true,
			'body'    => $resultCurl,
		];
	}

	/**
	 * @return string
	 */
	private function getTransactionCreateUrl()
	{

		$baseUrl = self::getServiceUrl();

		if($baseUrl) {
			return $baseUrl
				. '/'
				. self::MERCHANT
				. '/'
				. $this->merchantId
				. '/'
				. self::TRANSACTION;
		}

		return '';
	}

	/**
	 * @return string
	 */
	private function getServiceUrl()
	{

		if(isset(self::$serviceUrls[$this->environment])) {
			return self::$serviceUrls[$this->environment];
		}

		return '';
	}

	/**
	 * @return array
	 */
	public function getServiceInfo()
	{
		return $this->call(
			$this->getServiceInfoUrl(),
			Util::METHOD_REQUEST_GET,
			''
		);
	}

	/**
	 * @return string
	 */
	private function getServiceInfoUrl()
	{

		$baseUrl = self::getServiceUrl();

		if($baseUrl) {
			return $baseUrl
				. '/'
				. self::MERCHANT
				. '/'
				. $this->merchantId
				. '/'
				. self::SERVICE
				. '/'
				. $this->serviceId;
		}

		return '';
	}

	/**
	 * @param string $body
	 *
	 * @return array
	 */
	public function createRefund($body, $transactionUuid)
	{

		return $this->call(
			$this->getRefundCreateUrl($transactionUuid),
			Util::METHOD_REQUEST_POST,
			$body
		);
	}

	/**
	 * @param string $transactionUuid
	 *
	 * @return string
	 */
	private function getRefundCreateUrl($transactionUuid)
	{

		$baseUrl = $this->getServiceUrl();

		if($baseUrl) {
			return $baseUrl
				. '/'
				. self::MERCHANT
				. '/'
				. $this->merchantId
				. '/'
				. self::TRANSACTION
				. '/'
				. $transactionUuid
				. '/refund';
		}

		return '';
	}

	/**
	 * Creates full form with order data.
	 *
	 * @param array  $transaction
	 * @param string $submitValue
	 * @param string $submitClass
	 * @param string $submitStyle
	 *
	 * @return string
	 * @throws Exception
	 */
	public function buildOrderForm($transaction, $submitValue = '', $submitClass = '', $submitStyle = '')
	{

		if(!isset($transaction['body']['action'])) {

			throw new Exception(json_encode([
					'action' => 'apiBuildOrderForm',
					'error'  => 'Doesnt action exist',
				]
			));
		}

		return Util::createOrderForm(Util::parseStringToArray($transaction['body']['action']['contentBodyRaw']), $transaction['body']['action']['url'], $transaction['body']['action']['method'], $submitValue, $submitClass, $submitStyle);
	}
}
