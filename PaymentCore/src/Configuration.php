<?php

namespace Imoje\Payment;

/**
 * Class Configuration
 *
 * @package Imoje\Payment
 */
class Configuration
{

	const ROUTE_PAY = '/payment';
	const ROUTE_CHECK_PAYMENT_METHODS = '/check/methods';

	const METHOD_REQUEST_POST = 'POST';

	/**
	 * @var array
	 */
	private static $serviceUrls = [
		Util::ENVIRONMENT_PRODUCTION => 'https://paywall.imoje.pl',
		Util::ENVIRONMENT_SANDBOX    => 'https://sandbox.paywall.imoje.pl',
	];

	/**
	 * @var string
	 */
	private static $headerSignatureName = 'HTTP_X_IMOJE_SIGNATURE';

	/**
	 * @var string
	 */
	private static $serviceKey;

	/**
	 * @var string
	 */
	private static $serviceId;

	/**
	 * @var string
	 */
	private static $merchantId;

	/**
	 * @var bool
	 */
	private static $apiMode = false;

	/**
	 * @var string
	 */
	private static $environment;

	/**
	 * @var string
	 */
	private static $authorizationToken;

	/**
	 * @var array
	 */
	private static $serviceApiUrls = [
		Util::ENVIRONMENT_PRODUCTION => 'https://api.imoje.pl/v1',
		//Util::ENVIRONMENT_SANDBOX    => 'https://sandbox.api.imoje.pl', sandbox doesnt exist at this moment
	];

	/**
	 * @return string
	 */
	public static function getServiceKey()
	{
		return self::$serviceKey;
	}

	/**
	 * @param string $serviceKey
	 */
	public static function setServiceKey($serviceKey = '')
	{
		self::$serviceKey = trim((string) $serviceKey);
	}

	/**
	 * @return string
	 */
	public static function getServiceId()
	{
		return self::$serviceId;
	}

	/**
	 * @param string $serviceId
	 */
	public static function setServiceId($serviceId = '')
	{
		self::$serviceId = trim((string) $serviceId);
	}

	/**
	 * @return string
	 */
	public static function getMerchantId()
	{
		return self::$merchantId;
	}

	/**
	 * @param string $merchantId
	 */
	public static function setMerchantId($merchantId = '')
	{
		self::$merchantId = trim((string) $merchantId);
	}

	/**
	 * @return string
	 */
	public static function getEnvironment()
	{
		return self::$environment;
	}

	/**
	 * @param string $environment
	 */
	public static function setEnvironment($environment)
	{
		self::$environment = $environment;
	}

	/**
	 * @return string
	 */
	public static function getApiMode()
	{
		return self::$apiMode;
	}

	/**
	 * @param bool $apiMode
	 */
	public static function setApiMode($apiMode)
	{
		self::$apiMode = $apiMode;
	}

	/**
	 * @return string
	 */
	public static function getAuthorizationToken()
	{
		return self::$authorizationToken;
	}

	/**
	 * @param string $authorizationToken
	 */
	public static function setAuthorizationToken($authorizationToken)
	{
		self::$authorizationToken = $authorizationToken;
	}

	/**
	 * @param string $environment
	 *
	 * @return string|bool
	 */
	public static function getServiceUrl($environment)
	{

		if(isset(self::$serviceUrls[$environment])) {
			return self::$serviceUrls[$environment];
		}

		return false;
	}

	/**
	 * @return string
	 */
	public static function getHeaderSignatureName()
	{
		return self::$headerSignatureName;
	}

	/**
	 * @param string $environment
	 *
	 * @return string|bool
	 */
	public static function getServiceApiUrl($environment)
	{

		if(isset(self::$serviceApiUrls[$environment])) {
			return self::$serviceApiUrls[$environment];
		}

		return false;
	}

	/**
	 * @param string $environment
	 * @param string $mid
	 *
	 * @return string|bool
	 */
	public static function getTransactionCreateUrlApi($environment, $mid)
	{

		if(!($baseApiUrl = self::getServiceApiUrl($environment))) {
			return false;
		}

		return $baseApiUrl . '/merchant/' . $mid . '/transaction';
	}
}
