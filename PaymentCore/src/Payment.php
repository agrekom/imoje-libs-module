<?php

namespace Imoje\Payment;

/**
 * Class Payment
 *
 * @package Imoje\Payment
 */
class Payment
{
	/**
	 * Payment constructor.
	 *
	 * @param string $environment
	 * @param string $serviceKey
	 * @param string $serviceId
	 * @param string $merchantId
	 * @param bool   $apiMode
	 * @param string $authorizationToken
	 */
	public function __construct($environment, $serviceKey, $serviceId, $merchantId, $apiMode = false, $authorizationToken = '')
	{
		Configuration::setEnvironment($environment);
		Configuration::setServiceKey($serviceKey);
		Configuration::setServiceId($serviceId);
		Configuration::setMerchantId($merchantId);

		if($apiMode) {
			Configuration::setApiMode($apiMode);
			Configuration::setAuthorizationToken($authorizationToken);
		}
	}

	/**
	 * Creates full form with order data.
	 *
	 * @param array  $orderData
	 * @param string $submitValue
	 * @param string $url
	 *
	 * @return string
	 */
	public function buildOrderForm($orderData, $submitValue = '', $url = '')
	{

		if(!$url){
			$url = Util::getServiceUrl();
		}

		return Util::createOrderForm(
			Util::prepareOrderData($orderData),
			$submitValue,
			$url);
	}
}
