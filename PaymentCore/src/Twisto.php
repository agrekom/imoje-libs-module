<?php

namespace Imoje\Payment;

use DateTime;
use Twisto\Address;
use Twisto\Customer;
use Twisto\Item;
use Twisto\Order;

/**
 * Class Twisto
 *
 * @package Imoje\Payment
 */
class Twisto
{

	/**
	 * @var string[]
	 */
	private static $urlsScript = [
		Util::ENVIRONMENT_PRODUCTION => 'https://api.twisto.pl/v2/lib/twisto.js',
		Util::ENVIRONMENT_SANDBOX    => 'https://static.test.twisto.pl/api/v2/twisto.js',
	];

	/**
	 * @param string $environment
	 *
	 * @return string
	 */
	public static function getUrlScript($environment)
	{
		return self::$urlsScript[$environment];
	}

	/**
	 * @return string
	 */
	public static function getDataSandbox()
	{
		return json_encode([
			'status'         => 'accepted-verification-required',
			'transaction_id' => 'sandbox',
		]);
	}

	/**
	 * @param string $itemName
	 *
	 * @return bool|string
	 */
	public static function getItemName($itemName
	) {
		return substr($itemName, 0, 255);
	}

	/**
	 * @param array $paymentMethods
	 * @param bool  $twistoEnabled
	 *
	 * @return bool
	 */
	public static function isActive($paymentMethods, $twistoEnabled)
	{

		return isset(
				$paymentMethods['twisto'],
				$paymentMethods['twisto']['pk'],
				$paymentMethods['twisto']['sk']
			)
			&& $paymentMethods['twisto']
			&& $paymentMethods['twisto']['pk']
			&& $paymentMethods['twisto']['sk']
			&& $twistoEnabled;
	}

	/**
	 * @param string   $secretKey
	 * @param string   $total
	 * @param array    $cartData
	 * @param string   $email
	 * @param DateTime $dateCreated
	 * @param array    $previousOrders
	 *
	 * @return string
	 */
	public static function getPayload($secretKey, $total, $cartData, $email, DateTime $dateCreated, $previousOrders)
	{

		if(empty($cartData)) {
			return '';
		}

		require __DIR__ . '/../vendor/twistopayments/twisto.php/src/Twisto.php';

		$twistoItems = [];
		$twistoItems[] = new Item(
			Item::TYPE_SHIPMENT,
			self::getItemName($cartData['shipping']['name']),
			'shipment',
			1,
			round($cartData['shipping']['amount'], 2),
			$cartData['shipping']['vatRate']);

		if(isset($cartData['discount'])) {
			$twistoItems[] = new Item(
				Item::TYPE_DISCOUNT,
				self::getItemName($cartData['discount']['name']),
				'discount',
				1,
				-$cartData['discount']['amount'],
				$cartData['discount']['vatRate']);
		}

		foreach($cartData['items'] as $item) {

			$twistoItems[] = new Item(
				Item::TYPE_DEFAULT,
				self::getItemName($item['name']),
				$item['productId'],
				$item['quantity'],
				Util::multiplyValues($item['amount'], $item['quantity'], 2),
				$item['vatRate']
			);
		}

		$cartTotal = 0;
		foreach($twistoItems as $k => $v) {
			if(isset($v->price_vat)) {
				$cartTotal += $v->price_vat;
			}
		}

		if($cartTotal !== $total) {
			$twistoItems[] = new Item(
				Item::TYPE_ROUND,
				'round',
				'round',
				1,
				round($total - $cartTotal, 2),
				0
			);
		}

		$addressBilling = $cartData['addressBilling'];
		$addressShipping = $cartData['addressDelivery'];

		$data = new Order($dateCreated,
			new Address($addressBilling['name'],
				$addressBilling['street'],
				$addressBilling['city'],
				str_replace('-', '', $addressBilling['postalCode']),
				$addressBilling['countryCode'],
				$addressBilling['phone']),
			new Address($addressShipping['name'],
				$addressShipping['street'],
				$addressShipping['city'],
				str_replace('-', '', $addressShipping['postalCode']),
				$addressShipping['countryCode'],
				$addressShipping['phone']),
			$total, $twistoItems);

		$twisto = new \Twisto\Twisto();
		$twisto->setSecretKey($secretKey);

		return $twisto->getCheckPayload(new Customer($email), $data, $previousOrders);
	}
}
